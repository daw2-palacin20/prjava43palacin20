package prjava43palacin20;

import java.awt.*; 
import java.awt.event.*;
import java.sql.*;
import java.text.*;
import java.io.*;


public class Prjava43palacin20 extends Frame
implements ActionListener, WindowListener {
private TextField tfCount;
private int count = 0;
public Prjava43palacin20 () {
setLayout(new FlowLayout()); 
add(new Label("Counter")); 
tfCount = new TextField("0", 10); 
tfCount.setEditable(false);
add(tfCount); 
Button btnCount = new Button("Count"); 

add(btnCount); 
btnCount.addActionListener(this);
addWindowListener(this);
setTitle("nova finestra prjava43palacin20"); 
setSize(450, 100); 
setVisible(true); 
}
public static void main(String[] args) {
new Prjava43palacin20(); 
}
@Override
public void actionPerformed(ActionEvent evt) {
++count;
tfCount.setText(count + "");
}

@Override
public void windowClosing(WindowEvent e) {
System.exit(0); 
}

@Override
public void windowOpened(WindowEvent e) { }
@Override
public void windowClosed(WindowEvent e) { }
@Override
public void windowIconified(WindowEvent e) { }
@Override
public void windowDeiconified(WindowEvent e) { }
@Override
public void windowActivated(WindowEvent e) { }
@Override
public void windowDeactivated(WindowEvent e) { }
}
